import java.util.Scanner;

public class HelloWorld {


    static void order(String foodname){

        System.out.println("Thank you for ordering "+foodname+". It will be served as soon as possible");
    }
    public static void main(String[] args) {

        System.out.println("What would you like to order:");
        System.out.println("1.Tempura");
        System.out.println("2.Ramen");
        System.out.println("3.Udon");
        System.out.print("Your order:");

        Scanner userIn = new Scanner(System.in);
        int order=userIn.nextInt();
        userIn.close();



        if(order==1){
            order("Tempura");
        }
        if(order==2){
            order("Ramen");
        }
        if(order==3){
            order("Udon");
        }

    }
}
